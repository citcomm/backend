var express = require('express');
var router = express.Router();

let inputpage = require('../controllers/inputpage');

router.post('/', inputpage.create_measure);

module.exports = router;
