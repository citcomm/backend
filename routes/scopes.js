var express = require('express');
var router = express.Router();

let scopes = require('../controllers/scopes');

router.get('/', scopes.getScopesByName);

module.exports = router;
