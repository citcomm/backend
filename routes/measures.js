var express = require('express');
var router = express.Router();

let measures = require('../controllers/measures');

router.get('/', measures.getMeasures);
router.get('/:id', measures.getMeasuresByScope);

module.exports = router;
