var express = require('express');
var router = express.Router();

let measure = require('../controllers/measure');

router.get('/:id', measure.getMeasure);

module.exports = router;
