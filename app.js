var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var landingRouter = require('./routes/landing');
var inputpageRouter= require('./routes/inputpage');
var measureRouter= require('./routes/measure');
var measuresRouter= require('./routes/measures');
var scopesRouter= require('./routes/scopes');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// logs in development mode
app.use(logger('dev'));
// parse the input to a json
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
// parse cookies into req.cookies
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// TODO: Take care! XSS activated for every Client -> Vulnerability
// Why did we need this?
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//router
app.use('/', landingRouter);
app.use('/inputpage', inputpageRouter);
app.use('/measure', measureRouter);
app.use('/measures', measuresRouter);
app.use('/scopes', scopesRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send('error');
});

module.exports = app;
