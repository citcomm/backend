const dbPool = require('../db');
const util = require('../util');

exports.getMeasures = function(req, res, next) {

    try {
        // das ist die Hardcoded ID für Deutschland
        req.params.id = 1;
        exports.getMeasuresByScope(req, res, next);
    }
    catch(error){
        console.log('Error in get measures: ' + error);
        return res.status(500).send(error);
    };
};

exports.getMeasuresByScope = function(req, res, next) {
    // req: scope id from frontend
    let ScopeId = req.params.id;
    if (ScopeId === undefined){
        return res.status(500).send("No ScopeId provided!")
    }
    console.log("Enter getMeasuresByScope");

    try {
        let Query = `call getParentScopes(${ScopeId})`;
        dbPool.getConnection()
            .then(conn => {
                conn.query(Query)
                    .then(result => {
                        // all ids of scope and parent scopes. Scope levels under the given scope are returned null.
                        let scopeTrace = result[0][0];
                        let ScopeIds = "";
                        for(let i in scopeTrace) {
                            if (scopeTrace[i]){
                                ScopeIds = ScopeIds + scopeTrace[i].toString() + ", ";
                            }
                        }

                        // hack to remove the last kommata
                        ScopeIds = ScopeIds.slice(0, ScopeIds.length - 2);
                        console.log("Scopes:");
                        console.log(ScopeIds);

                        // query for all measures with one of the ScopeIds in their scope
                        let MeasureQuery = (
                            `SELECT me_gb.gb_rel_id, me.me_id
                                from measure_geltungsbereich_rel me_gb
                                left join measure me on me_gb.me_rel_id = me.me_id
                                where me_gb.gb_rel_id in (${ScopeIds});`
                        );

                        conn.query(MeasureQuery)
                            .then(result => {
                                let measureIds = [];
                                for(let i in result){
                                    console.log(i);
                                    measureIds.push(result[i].me_id);
                                }
                                util.measuresToJSON(measureIds).then(ret => {res.send(ret)});
                                return conn.end();
                            })
                    })
            })
    } catch (e) {
        console.log('Error in getMeasuresByScope: ' + e);
        return res.status(500).send(e);
    }
};

