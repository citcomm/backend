const dbPool = require('../db');
const util = require('../util');

exports.getMeasure = function(req, res, next) {
    let id = req.params.id;
    console.log("Enter getMeasure.");
    if (id === undefined){
            return res.status(500).send("No Measure id provided!")
        }

    try {
        util.measuresToJSON([id]).then(ret => {res.send(ret[0])});
    } catch (e) {
        console.log('Error in getMeasure: ' + e);
        return res.status(500).send(e);
    }

};
