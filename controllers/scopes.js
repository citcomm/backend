var dbPool = require('../db');
var util = require('../util');

exports.getScopesByName = function(req, res, next) {

    console.log('in getScopeByname');
    let searchterm = req.query.searchterm;
    if (searchterm === undefined){
        return res.status(500).send("No searchterm provided!")
    }

    let chars = searchterm.length;

    let sql="call searchSuggestion("+"'"+searchterm+"%',"+chars+");";

    try {
        dbPool.getConnection()
            .then(conn => {
                conn.query(sql)
                    .then(result => {
                        console.log('This is the result: ' + result);
                        res.send(util.scopesToJSON(result[0]));;
                        return conn.end();
                    })
            })
    } catch (e) {
        console.log('Error in getScopesByName: ' + e);
        return res.status(500).send(e);
    }
};
