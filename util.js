const dbPool = require('./db');

module.exports = {

    measureToJSON: function (measure, sources, scopes, tags) {
        const ScopeTypes = {
          1:   "municipality",
            2: "federation",
            3: "district",
            4: "county",
            5: "state",
            6: "bund",
        };

        console.log("Enter measureToJSON");
        let relevantSources = [];
        for(i in sources){
            if(sources[i].me_id == measure.me_id){
                relevantSources.push(sources[i].src_link);
            }
        }

        let relevantScopes = [];
        console.log(scopes)
        for(i in scopes){
            if(scopes[i].me_id == measure.me_id){
                let type = scopes[i].type;
                let scope = {
                    id: scopes[i].id,
                    name: scopes[i].name,
                    type: ScopeTypes[type],
                }
                relevantScopes.push(scope);
            }
        }

        let relevantTags = [];
        console.log("Die Tags: ")
        console.log(tags)
        for(i in tags){
            if(tags[i].me_id == measure.me_id){
                relevantTags.push(tags[i].tag_text);
            }
        }

        let ret = {
            id : measure.me_id,
            name: measure.me_name,
            decreeAuthority: measure.me_authority, // Erlassbehoerde
            fileReference: measure.me_file_reference,  // Aktenzeichen des Erlasses
            documentSource: relevantSources, // Link zum Quellen PdF
            scopes: relevantScopes,  // Geltungsbereiche der Measure
            decreeDate: measure.me_date_decree, // Datum des Erlasses
            validityDate: measure.me_date_begin, // Beginn der Measure
            endDate: measure.me_date_end,  // Ende der Measure
            legalNature: measure.legal_status,  // Rechtsnatur der Measure
            tags: relevantTags, // Schlagwörter
            references: [], // referenziert diese Measures (von)
            referencedBy: [], // wird von diesen Measures referenziert (auf)
            reason: measure.me_explanation, // Begründung der Maßnahme, entweder Text oder ein Link
        };
        return ret;
    },

    measuresToJSON: function (measureIds){
        console.log("measureisd:");
        console.log(measureIds);
        return new Promise(function (resolve, reject) {

            // measureIds: array of measureIds
            // returns: array of JSON measures

            if(!Array.isArray(measureIds) || !measureIds.length){
                console.log("Empty measureIds provided.")
                return resolve([]);
            }

            // convert ids to sql handlebar comma separated string
            let MeasureIdsString = "";
            for(let i in measureIds) {
                if(!(measureIds[i] == undefined)){
                    MeasureIdsString = MeasureIdsString + measureIds[i].toString() + ", ";
                }
            }
            MeasureIdsString = MeasureIdsString.slice(0, MeasureIdsString.length - 2);

            // define a list to be returned later
            let ret = [];
            console.log("MeasureIds as string: ");
            console.log(MeasureIdsString);

            // query the easy accessible information
            let baseQuery = (
                `SELECT me.me_id, me.me_name, me.me_file_reference, me.me_authority, me.me_date_decree, me.me_date_begin, me.me_date_end, me.me_legal_status, me.me_explanation 
                     from measure me
                     where me.me_id in (${MeasureIdsString});`
            );

            try {
                dbPool.getConnection()
                    .then(conn => {
                        conn.query(baseQuery)
                            .then(basicMeasure => {
                                console.log('This is the basic: ' + basicMeasure);
                                // now, query the associated sources
                                let sourceQuery = (
                                    `SELECT me.me_id, src.src_id, src.src_link
                                     from measure me
                                     left join source src on me.me_id = src.src_ref_me
                                     where me.me_id in (${MeasureIdsString});`
                                );
                                conn.query(sourceQuery)
                                    .then(sources => {
                                        console.log('This is the source: ' + sources);

                                        // Select scope with type by measure id.
                                        let scopeQuery = (
                                            `SELECT mgr.me_rel_id as me_id, scopes.id, scopes.name, scopes.type
                                            from (
                                                  SELECT bund_id id, bund_name name, 6 type from bund
                                                  UNION SELECT land_id id, land_name name, 5 type from bundesland
                                                  UNION SELECT regbez_id id, regbez_name name, 4 type from regierungsbezirk
                                                  UNION SELECT kreis_id id, kreis_name name, 3 type from kreis
                                                  UNION SELECT gv_id id, gv_name name, 2 type from gemeindeverband
                                                  UNION SELECT gem_id id, gem_name name, 1 type from gemeinde
                                                 ) scopes
                                            join measure_geltungsbereich_rel mgr on mgr.gb_rel_id = scopes.id
                                            where mgr.me_rel_id in (${MeasureIdsString});`
                                            );

                                        conn.query(scopeQuery)
                                            .then(scopes => {

                                                let tagQuery = `SELECT me_rel_id as me_id, tag_text from measure_tag_rel join tag on tag_rel_id = tag_id where me_rel_id in (${MeasureIdsString});`;

                                                conn.query(tagQuery)
                                                    .then(tags => {

                                                        console.log("Scopes: ");
                                                        console.log(scopes);
                                                        for (i in basicMeasure) {
                                                            if(i !== "meta") {
                                                                ret.push(module.exports.measureToJSON(basicMeasure[i], sources, scopes, tags));
                                                            }
                                                        }
                                                        console.log("Die Tags: ")
                                                        console.log(tags);
                                                        console.log("Im util ist der return: ");
                                                        console.log(ret);
                                                        console.log("... gewesen.");
                                                        conn.end();
                                                        return resolve(ret);
                                            });
                                        });
                                    });
                                });
                            });
            } catch (e) {
                console.log('Error in measuresToJSON: ' + e);
                return res.status(500).send(e);
            }
        });
    },


    parseScopeToJSON: function (scopeRow) {
        let obj = {
            id: scopeRow.id,
            municipality: scopeRow.gem_name,
            federation:  scopeRow.gv_name,
            district: scopeRow.kreis_name,
            county: scopeRow.regbez_name,
            state: scopeRow.land_name,
            country: scopeRow.bund_name
        }
        return obj;
    },

    scopesToJSON: function (scopes){
        let scopesArr = [];
        for(var s in scopes){
            scopesArr.push(this.parseScopeToJSON(scopes[s]));
        }
        return scopesArr;
    },

};
