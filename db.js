const mariadb = require('mariadb');
const dbPool = mariadb.createPool({
    host: 'citcomm.de',
    user:'cores',
    password: 'BlubberPasswort123',
    connectionLimit: 5,
    database: "cores",
    port: 3306,
});

console.log('Databasepool is established.');

module.exports = dbPool;
